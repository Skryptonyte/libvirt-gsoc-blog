# Google Summer of Code 2024 - Progress Report

This is a blog to keep track of my current progress. I might make a proper Github Pages site later, but for now a README will do.

My current project is to build a XML-to-QEMU-command-line generation fuzzer for Libvirt. Building a fuzzer for highly structured grammar like Libvirt's XML domain schema is not trivial. I plan to use LibFuzzer along with the libprotobuf-mutator to construct a grammar-aware fuzzer. The idea is that the Libvirt XML domain grammar can be defined using protobuf files. This protobuf data gets deserialized into XML and input into our test functions within the harness.

One problem is that Libvirt's XML domain schema gets updated fairly frequently, so automation of the grammar generation process is desirable. I have a number of ideas, with one being writing our own RNGSchema to Protobuf converter, which in theory shouldn't be too difficult.

Another possible issue is dealing with the weird build setup of libprotobuf-mutator. This is particularly important as I will probably have to integrate this into Libvirt's own system.

## Pre-GSOC

Merged patches:

1. https://lists.libvirt.org/archives/list/devel@lists.libvirt.org/thread/LEAENTOC4GA5DJGGEII4A3BS6YYGO7IP/ - Implement support for new filesystem driver type ``mtp``
2. https://lists.libvirt.org/archives/list/devel@lists.libvirt.org/thread/C4LOFSW5LDEXPIDSJ66YMHHPQTSJDH65/ - Improve usb-net support by implementing automatic USB address allocation
3. https://lists.libvirt.org/archives/list/devel@lists.libvirt.org/thread/CSG2W6D4SWBQTG6RF4MT5KJJJKZX627I/#%23F7CUJXXX6OI6VWJUKOM7BK5WHPOOWHAR - Added support for sound model ``virtio`` based on the newly implemented virtio-sound-* devices from QEMU 8.2 onwards.

During this time, I also played around with mostly libFuzzer-based approaches and made some small PoCs.

## Community Bonding Period

I had a brief introductory chat with my mentors, Michal Privoznik and Martin Kletzander on 6th May.

During this period, I spent a bit of time studying the QEMU driver code outside the command line generation portions.

Merged patches:

1. https://lists.libvirt.org/archives/list/devel@lists.libvirt.org/thread/AI6PXVLMFASPOBQWZI2WRAUFA3VB5BBL/#3K334TJYHXWTS4TKWBOTSZE4WX7HPCOU - Explicitly ensure USB address to usb-net devices that get hotplugged.
2. https://lists.libvirt.org/archives/list/devel@lists.libvirt.org/thread/AIAS5CWQA6VQKEIF453NXBFKVJW5E3RX/#R7YCMWL5JPR2R2RCWYXPAII6BM2NZFCU - Implement support for hotplugging and unhotplugging of evdev input devices

I've also continued investigating other potential fuzzing approaches besides the one I had outlined in my proposal. I found another project, ``cryptsetup`` which has a similar project structure and build setup to libvirt. They have implemented fuzzing engines based on libprotobuf-mutator. I may take cues from that project.

## Week 1

I started off by setting up the initial fuzzing target in `tests/fuzz`. This involved modifying the meson build in / and tests/ to compile code in our new directory. I also
had to add checks for the libprotobuf_mutator and protobuf dependencies as well as the protoc tool. If all of these are satisifed, it will display `Fuzzing: YES` under the
test summary after the build setup. This is also the first time that C++ code is getting introduced into the libvirt code base so we have to explicitly tell meson that
cpp is included in our compilation.

The initial compilation failed for three reasons:

1. Conflicting identifiers of typedef struct and typedef enum in netdev_vport_profile_conf.c (Easy to fix).
2. Various C++ keyword conflicts here and there (Easy to fix).
3. Expected function declaration error in multiple function definitions, specifically those having multiple function attributes along with G_NO_INLINE. I removed G_NO_INLINE
   as a temporary workaround.

Now, the compilation passed but I ran into linker errors when trying to use functions from libvirt's headers. I was not too familiar with mixed C/C++ environments at the time.
C++ does name mangling very differently from C and expects the same mangling from the C objects which is why it was unable to find the functions during linkage. You are supposed
to cover all your header file includes with `extern "C"` which simply tells the compiler that the objects to be linked are C objects. It ended up spitting out some template errors somewhere deep in the libxml2 headers. Explicitly defining libxml2 headers before the extern declaration solved the issue. Static linking C libraries to C++ executables does not
seem to work at all, so I made a separate shared library of testqemuutils for the fuzzer.

After going through the compilation and linkage trouble, I set up a simple harness that calls `virDomainDefParseString` on
a hardcode XML string as a sanity check. However, it ended up crashing somewhere inside the aforementioned function. This particular issue had me stumped for a while. I noticed it was crashing on virError related functions, but didn't know why.
As an ugly workaround, I just mocked those functions with LD_PRELOAD. It still did fail somewhere in post parsing. I realized that it was because the QEMU capabilities cache was not setup. I used qemuxmlconftest as a reference to implement
this in the fuzzer. Finally the fuzzer was working.

I started writing a protobuf to XML converter that could handle arbitrary tags and attributes. A trick I did was to annotate protobuf fields with `T_`, `A_` and `V_` to distinguish which fields represent tags, attributes, or general values contained in between tags respectively. My code heavily
abuses protobuf reflection APIs to iterate through all protobuf fields and incrementally build an XML string in memory.

I further optimized my fuzzer by putting all initializations in static variables, so that the driver and test structures do not get re-initialized in every iteration. This nearly doubled the fuzzer speed.

Back to the virError issue, I later found out it was because I neglected to run virErrorInitialize(). Now, no mocking was needed.

I extended the harness further by adding command line generation related functions similar to what qemuxmlconftest does. It
worked right out of the gate without any additional set up.

I incremetally wrote more protobuf messages to cover more parts of the libvirt XML domain format. This is when I noticed
libvirt wasn't picking up as many coverage points as it should have even though I verified that it could reach certain functions correctly. I realized that I had applied `-fsanitize=fuzzer` only to the executable. I was supposed to introduce SanitizerCoverage to every other C object file using `-fsanitize=fuzzer-no-link`. After re-running setup, I encountered hundreds of link errors. I took a look at how ASAN instrumented libvirt builds were setup in the CI and found that they passed `-b_lundef=false` which solved the issue. Now, I had a fully instrumented libvirt setup and the fuzzer was able to pick up coverage and even log all the newly discovered functions within the libvirt code base.

To convert enums to XML attributes, I directly take their names from their respective EnumValueDescriptors and construct the equivalent XML attribute. This approach has a drawback in that you cannot write enum values containing special characters ('usb-net', 'virtio-mmio', etc.). As a work around, I used the enum protobuf extension to basically append a string field to an EnumValueDescriptor that acts as an alternate name to be used in XML generation.

Following all this, I wrote even more protobuf grammar to exercise more code paths in libvirt in the hopes of finding bugs.

At the end of the week, I discovered two bugs which I have reported to my mentors. I do not know how much of a security risk they are so I am not going to mention them publicly for now.

At this stage, the fuzzer is fully functional but there is still work left to do.
